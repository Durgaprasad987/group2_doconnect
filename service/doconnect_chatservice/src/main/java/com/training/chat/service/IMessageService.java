package com.training.chat.service;

import java.util.List;

import javax.validation.Valid;

import com.training.chat.dto.MessageDTO;

public interface IMessageService {

	public MessageDTO sendMessage(@Valid MessageDTO messageDTO);

	public List<MessageDTO> getMessage();

}
