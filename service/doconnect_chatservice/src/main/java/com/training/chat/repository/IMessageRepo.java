package com.training.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.chat.entity.Message;

@Repository
public interface IMessageRepo extends JpaRepository<Message, Long> {

}
